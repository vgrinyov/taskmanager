package com.grinyov.crudapp.controller;

/**
 * Created by grinyov on 06.12.16.
 */
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;

@Controller

public class HomeController {

    @RequestMapping("/home")

    public String home() {

        return "index";

    }

}